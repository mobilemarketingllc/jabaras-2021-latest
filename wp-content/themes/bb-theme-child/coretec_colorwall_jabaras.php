<?php

/*
Template Name: Coretec Colorwall Jabaras

*/
 get_header(); 
 ?>
<h1 class="none_h1"> coretec colorwall </h1>
<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
			<?php
			echo do_shortcode('[fl_builder_insert_layout slug="colorwall-final"]');
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>