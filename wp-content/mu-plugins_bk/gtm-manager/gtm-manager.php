<?php

/*
Plugin Name: MM Google Tag Manager
Plugin URI:  http://mobile-marketing.agency
Description: For managing your sites Google Tag Manager script insertion.
Version:     0.1.5
Author:      mobile-marketing.agency
Author URI:  http://mobile-marketing.agency
*/


/**
 * Class for adding a new field to the options-general.php page
 */
class Add_Settings_Field {

	// Class constructor
	public function __construct() {
		add_action( 'admin_init' , array( $this , 'register_fields' ) );
	}

	// Add new fields to wp-admin/options-general.php page
	public function register_fields() {
		register_setting( 'general', 'gtm_script_insert', 'esc_attr' );
		add_settings_field(
			'extra_blog_desc_id',
			'<label for="gtm_script_insert_id">' . __( 'Google Tag Manager container ID (GTM-XXXX)' ,
			'gtm_script_insert' ) . '</label>',
			array( $this, 'fields_html' ),
			'general'
		);
	}

	// HTML for extra settings
	public function fields_html() {
		$value = get_option( 'gtm_script_insert', '' );
		echo '<input type="text" id="gtm_script_insert_id" name="gtm_script_insert" value="' . esc_attr( $value ) . '" />';
	}

}
new Add_Settings_Field();



// Add scripts to wp_head()
function gtm_head_script() {
	$id = get_option( 'gtm_script_insert', '' );
	if (!empty($id)) { ?>
	 
	 	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer', '<?php echo $id; ?>' );</script>
		<!-- End Google Tag Manager -->

	 <?php } ?>
<?php }
add_action( 'wp_head', 'gtm_head_script' );



/*
* This solution makes two assumptions:
* The theme’s opening <body> tag ends with <?php body_class(); ?>.
*    For most scenarios this should be the case
* There isn’t any other plugin which filters the body_class hook and has a lower priority (the default priority is 10 – this is set to 10000).
*/
add_filter( 'body_class', 'gtm_body_script', 10000 );
function gtm_body_script( $classes ) {
$id = get_option( 'gtm_script_insert', '' );

	if (!empty($id)) {
		$block = <<<BLOCK
 
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=$id"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->
 
BLOCK;
 
	$classes[] = '">' . $block . '<br style="display:none';      
	return $classes;
	}
}

?>